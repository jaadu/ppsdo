/*
 * Author: Einar Largenius
 *
 * Created on den 20 maj 2020, 21:05
 */

#include "configuration.h"
#include <xc.h>
#include <stdint.h>

// Timer to keep overflow from builtin timer
uint8_t timer_u = 0;

// Temp variables to keep oscillations per second, from biggest to
// smallest. Default values are approximetely a second.
uint8_t one_sec_u = 153;
uint8_t one_sec_l = 127;
uint8_t one_sec_h = 105;

#define HIGH_COUNT 5
uint8_t output_high_cnt = 0;

/*
 * Forward the signal to output and store the intervalls.
 */
void have_signal() {
	if (PIR1 & 0b00000001) {	// Timer1 interrupt
		timer_u++;
		PIR1bits.TMR1IF = 0;	// Reset interrupt
		if (output_high_cnt) {
			output_high_cnt--;
		}
		else {
			PORTBbits.RB6 = 0;
		}
	}
	if (PIR1 & 0b00000100) {	// Capture interrupt (RC5)
		one_sec_u = timer_u + 1;
		one_sec_l = 255 - CCPR1L;
		one_sec_h = 255 - CCPR1H;
		timer_u = 0;

		PIR1bits.CCP1IF = 0;
		output_high_cnt = HIGH_COUNT;
		PORTBbits.RB6 = 1;
	}
}

/*
 * Use stored time for pulses.
 */
void no_signal() {
	if (output_high_cnt) {
		output_high_cnt--;
	}
	else {
		PORTBbits.RB6 = 0;
	}

	timer_u++;
	if (timer_u >= one_sec_u) {
		TMR0H = one_sec_h;
		TMR0L = one_sec_l;
		timer_u = 0;

		output_high_cnt = HIGH_COUNT;
		PORTBbits.RB6 = 1;
	}
	PIR1bits.TMR1IF = 0;	// Reset interrupt
	PIR1bits.CCP1IF = 0;
}

/*
 *
 */
void __interrupt(low_priority) isr() {
	PORTCbits.RC6 = ~PORTCbits.RC6; // Debug
	if (PORTCbits.RC0 == 0) {	// When the GPS doesn't have a connection
		have_signal();
	}
	else {
		no_signal();
	}
}

/*
 *
 */
void __interrupt(high_priority) isr_high() {
	TMR0H = one_sec_h;
	TMR0L = one_sec_l;
	timer_u = 0;
	INTCONbits.INT0IF = 0;
	__asm("BCF 0xF, 1, 0");
	__asm("RETFIE");
}

/*
 * Setup variables
 */
void configure() {
	RCON	= 0b10000000;
	T1CON   = 0b10000000;
	CCP1CON = 0b00000100;
	PIE1	= 0b00000101;
	IPR1	= 0b00000000;
	INTCON2 = 0b11000000;
	INTCON  = 0b11010000;

	CCPR1H = one_sec_h;
	CCPR1L = one_sec_l;

	ANSELbits.ANS4  = 0;	// Use digital inputs
	ANSELHbits.ANS8 = 0;

	TRISCbits.RC5 = 1;		// Capture interrupt
	TRISCbits.RC0 = 1;		// When the GPS has connection
	TRISBbits.RB6 = 0;		// Output Pulse
	TRISCbits.RC6 = 0;		// Debug
}

int main(int argc, char** argv) {
	configure();
	T1CON |= 0b00000001;
	uint16_t testing = 0;
	while (1) {
		/* testing++; */
		/* if (!testing) { */
		/* 	PORTCbits.RC6 = ~PORTCbits.RC6; */
		/* } */
		// Absolutely nothing...
	}
	return (EXIT_SUCCESS);
}

/* Local Variables: */
/* tab-width: 4 */
/* indent-tabs-mode: t */
/* End: */
